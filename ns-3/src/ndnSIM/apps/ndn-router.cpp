#include "ndn-router.hpp"
 
#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"
#include "ns3/callback.h"
#include "ns3/string.h"
#include "ns3/boolean.h"
#include "ns3/uinteger.h"
#include "ns3/integer.h"
#include "ns3/double.h"
 
#include "ns3/ndnSIM/helper/ndn-stack-helper.hpp"
#include "ns3/ndnSIM/helper/ndn-fib-helper.hpp"
#include "ns3/ndnSIM/model/ndn-common.hpp"
 
#include "model/ndn-app-face.hpp"
 
#include "ns3/random-variable-stream.h"
 
 
NS_LOG_COMPONENT_DEFINE("Router");
 
namespace ns3 {
namespace ndn {
 
 // Necessary if you are planning to use ndn::AppHelper
NS_OBJECT_ENSURE_REGISTERED(Router);
 
TypeId
Router::GetTypeId()
{
  static TypeId tid = TypeId("ns3::ndn::Router")
  .SetGroupName("Ndn")
  .SetParent<MultimediaConsumer<FileConsumerCbr>>()
  .AddConstructor<Router>();

  return tid;
}
 
Router::Router() : MultimediaConsumer<FileConsumerCbr>()
{
}
 
void
Router::StartApplication()
{
  NS_LOG_FUNCTION_NOARGS();

  MultimediaConsumer<FileConsumerCbr>::StartApplication();
 
  // equivalent to setting interest filter for "/prefix" prefix
  ndn::FibHelper::AddRoute(GetNode(), "/myprefix", m_face, 0); 
  
  m_isRouter = true;
  
}
 
void
Router::StopApplication()
{
  NS_LOG_FUNCTION_NOARGS();
 
  MultimediaConsumer<FileConsumerCbr>::StopApplication();
}
 
void
Router::OnInterest(std::shared_ptr<const ndn::Interest> interest)
{
  ndn::App::OnInterest(interest); // forward call to perform app-level tracing
  
  NS_LOG_DEBUG("Received Interest: " << interest->getName());

  /*shared_ptr<Interest> interest2 = make_shared<Interest>();
  interest2->setNonce(m_rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
  interest2->setName(interest->getName());
  time::milliseconds interestLifeTime(m_interestLifeTime.GetMilliSeconds());
  interest2->setInterestLifetime(interestLifeTime);*/

  m_transmittedInterests(interest, this, m_face);
  m_face->onReceiveInterest(*interest);

}


void
Router::OnData(std::shared_ptr<const ndn::Data> data)
{
  
  NS_LOG_DEBUG("Received Data: " << data->getName());
  

  if (!AreAllSeqReceived()){
   MultimediaConsumer<FileConsumerCbr>::OnData(data);
   return; 
  } else if (!m_mpdParsed){
   OnFileReceived(0,0);
  }  
  else if (m_currentDownloadType == Segment){

   
  int pos = data->getName().getSubName(4,1).toUri().find(".");
  
  if (pos != std::string::npos){ 
  std::string seqNo = data->getName().getSubName(4,1).toUri().substr(9, pos - 9);
 
  	if (lastSeqNo != std::stoi(seqNo)) {
		
    	  if (lastSeqNo == NULL) {
          std::string prefix = data->getName().getPrefix(4).toUri().substr(0,26);
	  content.first = prefix;
	  content.second = reputation;
          if (std::stoi(seqNo) != 1) RateData(std::stoi(seqNo), stoi(seqNo));
          }
 	  else {
          RateData(std::stoi(seqNo) - lastSeqNo, stoi(seqNo));
          content.second = reputation;
          }
	  lastSeqNo = std::stoi(seqNo);	  
	}
  
  } // if pos  
  } //else if
  

}

void
Router::OnFileReceived(unsigned status, unsigned length){

 
  MultimediaConsumer<FileConsumerCbr>::OnFileReceived(status, length);
  NS_LOG_DEBUG("Parsed whole MPD File! ");
  
  lastSeqNo = NULL;
  gaps=0; 
  reputation = 3.5;
}


void Router::RateData(int seqNodiff, int nooflastseg){

  double gapratio;
  float update;  

  if (reputation == 10) update = 0;
  else if (seqNodiff == 1) update=1;
  else {
   gaps++;
   gapratio = static_cast<double>(gaps)/nooflastseg;
   update = -0.2 * exp (1 - gapratio); // -θ* exp(α * (1 - gapratio)), θ = 0.2, α = 1
  }
  
  NS_LOG_DEBUG("Update value = " << update);
  reputation = 0.85 * reputation + update;
  if (reputation > 10) reputation = 10;
  NS_LOG_DEBUG("New reputation = " << reputation);

  return;

}
 
 
} //namespace ndn
} // namespace ns3
