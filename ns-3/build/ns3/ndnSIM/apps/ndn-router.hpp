/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 Christian Kreuzberger and Daniel Posch, Alpen-Adria-University 
 * Klagenfurt
 *
 * This file is part of amus-ndnSIM, based on ndnSIM. See AUTHORS for complete list of 
 * authors and contributors.
 *
 * amus-ndnSIM and ndnSIM are free software: you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * amus-ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * amus-ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef NDN_ROUTER_H
#define NDN_ROUTER_H

#include "ns3/ndnSIM/apps/ndn-app.hpp"
//#include "ns3/ndnSIM/apps/ndn-file-consumer.hpp"
#include "ns3/ndnSIM/apps/ndn-multimedia-consumer.hpp"

#include "ns3/random-variable-stream.h"


#include "libdash.h"
#include "multimedia-player.h"

#include "boost/algorithm/string/predicate.hpp"
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>



using namespace dash::mpd;


namespace ns3 {
namespace ndn {

/**
 * @ingroup ndn-apps
 * @brief Ndn application for sending out Interest packets at a "constant" rate (Poisson process)
 */

class Router : public MultimediaConsumer<FileConsumerCbr> {
 
public:
  static TypeId
  GetTypeId();

  Router();
  virtual ~Router(){};

  
  void
  OnInterest(std::shared_ptr<const ndn::Interest> interest);
  
  void
  OnData(std::shared_ptr<const ndn::Data> data);

  void RateData(int seqNodiff, int nooflastseg);

  std::pair <std::string, float> content;
  
  int gaps;
  float reputation;
  

protected:
  Ptr<UniformRandomVariable> m_rand;
  Time m_interestLifeTime;

  unsigned int lastSeqNo;

  // inherited from Application base class.
  virtual void
  StartApplication();

  virtual void
  StopApplication();

  virtual void
  OnFileReceived(unsigned status, unsigned length);

};


} // namespace ndn
} // namespace ns3

#endif
