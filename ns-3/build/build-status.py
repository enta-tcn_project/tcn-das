#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/subdir/ns3-dev-subdir-debug', 'build/scratch/ns3-dev-ndn-svc-multimedia-debug', 'build/scratch/ns3-dev-ndn-avc-multimedia-debug', 'build/scratch/ns3-dev-scratch-simulator-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

